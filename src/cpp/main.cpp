#define GLFW_INCLUDE_NONE

#include <GLFW/glfw3.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <iostream>

#include "loaders/text/load_text.h"
#include "utils/opengl.h"
#include "camera.h"
#include "objects/textured_model.h"
#include "objects/skybox.h"

const float PI = glm::pi<float>();

// The field of view
const float FOV_Y = 70;

const float CAMERA_ROTATION_SPEED = 60;

const float CAMERA_MOVEMENT_SPEED_LOW = 6;
const float CAMERA_MOVEMENT_SPEED_HIGH = 60;

float cameraMovementSpeed = CAMERA_MOVEMENT_SPEED_LOW;

bool spotLightEnabled = true;

GLFWwindow* window;

// The viewport width
int width = 800;

// The viewport height
int height = 640;

// The viewport aspect
float aspect = (float) width / (float) height;

// The mouse x coordinate
double mouseX = 0;

// The mouse y coordinate
double mouseY = 0;

// Shows whether the mouse is captured
bool mouseCaptured;

// The elapsed time since the GLFW initialization in seconds
float t;

// The elapsed time since the last frame in seconds
float dt;

struct AmbientLightUniform {
    GLuint color;
};

struct DirectionalLightUniform {
    GLuint color;
    GLuint direction;
};

struct PointLightUniform {
    GLuint color;
    GLuint position;
    GLuint attenuation;
    GLuint enabled;
};

struct SpotLightUniform {
    GLuint color;
    GLuint position;
    GLuint direction;
    GLuint innerAngleCos;
    GLuint outerAngleCos;
    GLuint attenuation;
    GLuint enabled;
};

// The OpenGL variables for the skybox program
GLuint skyboxProgram;
struct {
    GLuint u_forward;
    GLuint u_clipPlaneRight;
    GLuint u_clipPlaneUp;
    GLuint u_texture;
} skyboxUniforms;

// The OpenGL variables for the texture program
GLuint textureProgram;
struct {
    GLuint u_modelMatrix;
    GLuint u_normalMatrix;
    GLuint u_matrix;
    GLuint u_cameraPosition;
    GLuint u_specular;
    GLuint u_shininess;
    AmbientLightUniform u_ambientLight;
    DirectionalLightUniform u_directionalLight;
    PointLightUniform u_pointLights[10];
    SpotLightUniform u_spotLight;
    GLuint u_texture;
} textureUniforms;

Camera camera(0, 2, 0);

glm::mat4 projectionViewMatrix;

Skybox skybox("../res/image/skybox/right.png",
              "../res/image/skybox/left.png",
              "../res/image/skybox/up.png",
              "../res/image/skybox/down.png",
              "../res/image/skybox/front.png",
              "../res/image/skybox/back.png");


TexturedModel scene("../res/model/scene/scene.obj");

TexturedModel lantern("../res/model/scene/lantern.obj");

glm::vec3 lanternsPositions[] {
    glm::vec3(-13, 0, -25),
    glm::vec3(-13, 0, 25),
    glm::vec3(13, 0, 25),
    glm::vec3(13, 0, -25)
};

glm::vec3 lanternsRotations[] {
    glm::vec3(0, 0, 0),
    glm::vec3(0, 0, 0),
    glm::vec3(0, PI, 0),
    glm::vec3(0, PI, 0),
};

// The point lights are translated relative to the lanterns by (1.5, 5.5, 0)
glm::vec3 pointLightsPositions[] {
        glm::vec3(-11.5, 5.5, -25),
        glm::vec3(-11.5, 5.5, 25),
        glm::vec3(11.5, 5.5, 25),
        glm::vec3(11.5, 5.5, -25)
};

void initializeScene();

void initializeSkyboxProgram();

void initializeTextureProgram(GLuint lightingShader);

void resizeViewport();

void update();

void updateControls();

void updateProjectionViewMatrix();

void draw();

void glfwErrorCallback(int error, const char* description) {
    std::cerr << "The GLFW function call completed with errors:\n" << description << std::endl;
}

void glfwCursorPosCallback(GLFWwindow* window, double xpos, double ypos) {
    if (mouseCaptured) {
        camera.rotateHorizontally((xpos - mouseX) * glm::radians(0.1f));
        camera.rotateVertically(-(ypos - mouseY) * glm::radians(0.1f));

        mouseX = xpos;
        mouseY = ypos;
    }
}

void glfwMouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        mouseCaptured = true;
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        glfwGetCursorPos(window, &mouseX, &mouseY);
    }
}

void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        mouseCaptured = false;
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
    else if (key == GLFW_KEY_LEFT_SHIFT && action == GLFW_PRESS) {
        if (cameraMovementSpeed == CAMERA_MOVEMENT_SPEED_LOW) cameraMovementSpeed = CAMERA_MOVEMENT_SPEED_HIGH;
        else cameraMovementSpeed = CAMERA_MOVEMENT_SPEED_LOW;
    }
    else if (key == GLFW_KEY_1 && action == GLFW_PRESS) {
        spotLightEnabled = !spotLightEnabled;
    }
}

int main() {
    glfwSetErrorCallback(glfwErrorCallback);

    if (!glfwInit()) return -1;

    // Set the OpenGL version window hints
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    // Set the OpenGL context window hints (required for macOS)
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(width, height, "Task 4", nullptr, nullptr);

    // Close the application if the window isn't created
    if (!window) {
        glfwTerminate();
        return -1;
    }

    // Set the callbacks for controls
    glfwSetCursorPosCallback(window, glfwCursorPosCallback);
    glfwSetMouseButtonCallback(window, glfwMouseButtonCallback);
    glfwSetKeyCallback(window, glfwKeyCallback);

    // Enable the raw mouse motion for the camera control
    if (glfwRawMouseMotionSupported()) glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
    else std::cout << "Raw mouse motion isn't supported" << std::endl;

    glfwMakeContextCurrent(window);

    // Limit the frame rate to a screen refresh rate
    glfwSwapInterval(1);

    GLenum glewStatus = glewInit();

    if (glewStatus != GLEW_OK) {
        std::cerr << "The GLEW initialization completed with errors:\n" << glewGetErrorString(glewStatus) << std::endl;
        glfwTerminate();
        return -1;
    }

    try {
        initializeScene();
    }
    catch (const std::exception &e) {
        // Close the application if there is an error while loading the files, compiling the shaders or linking the program
        std::cerr << e.what() << std::endl;
        glfwTerminate();
        return -1;
    }

    // Get the initial time
    t = glfwGetTime();

    while (!glfwWindowShouldClose(window)) {
        // Don't update and draw the scene if the program window is minimized
        if (glfwGetWindowAttrib(window, GLFW_ICONIFIED)) {
            glfwWaitEvents();
            continue;
        }

        // Calculate the elapsed time
        float nt = glfwGetTime();
        dt = nt - t;
        t = nt;


        resizeViewport();
        update();
        draw();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

void initializeScene() {
    std::string lightingShaderSource = loadText("../src/glsl/lighting.glsl");

    GLuint lightingShader = createShader(lightingShaderSource, GL_FRAGMENT_SHADER);

    initializeSkyboxProgram();
    initializeTextureProgram(lightingShader);

    skybox.initialize();

    scene.initialize();

    lantern.initialize();
}

void initializeSkyboxProgram() {
    std::string vertexShaderSource = loadText("../src/glsl/skybox-vertex.glsl");
    std::string fragmentShaderSource = loadText("../src/glsl/skybox-fragment.glsl");

    GLuint vertexShader = createShader(vertexShaderSource, GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader(fragmentShaderSource, GL_FRAGMENT_SHADER);

    skyboxProgram = createProgram(vertexShader, fragmentShader);

    skyboxUniforms.u_forward = glGetUniformLocation(skyboxProgram, "u_forward");
    skyboxUniforms.u_clipPlaneRight = glGetUniformLocation(skyboxProgram, "u_clipPlaneRight");
    skyboxUniforms.u_clipPlaneUp = glGetUniformLocation(skyboxProgram, "u_clipPlaneUp");
    skyboxUniforms.u_texture = glGetUniformLocation(skyboxProgram, "u_texture");

    glUseProgram(skyboxProgram);

    glUniform1i(skyboxUniforms.u_texture, 0);
}

void initializeTextureProgram(GLuint lightingShader) {
    std::string vertexShaderSource = loadText("../src/glsl/texture-vertex.glsl");
    std::string fragmentShaderSource = loadText("../src/glsl/texture-fragment.glsl");

    GLuint vertexShader = createShader(vertexShaderSource, GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader(fragmentShaderSource, GL_FRAGMENT_SHADER);

    textureProgram = createProgram({ vertexShader, fragmentShader, lightingShader });

    textureUniforms.u_modelMatrix = glGetUniformLocation(textureProgram, "u_modelMatrix");
    textureUniforms.u_normalMatrix = glGetUniformLocation(textureProgram, "u_normalMatrix");
    textureUniforms.u_matrix = glGetUniformLocation(textureProgram, "u_matrix");
    textureUniforms.u_cameraPosition = glGetUniformLocation(textureProgram, "u_cameraPosition");

    textureUniforms.u_specular = glGetUniformLocation(textureProgram, "u_specular");
    textureUniforms.u_shininess = glGetUniformLocation(textureProgram, "u_shininess");

    TexturedModel::u_specular = textureUniforms.u_specular;
    TexturedModel::u_shininess = textureUniforms.u_shininess;

    textureUniforms.u_ambientLight.color = glGetUniformLocation(textureProgram, "u_ambientLight.color");

    textureUniforms.u_directionalLight.color = glGetUniformLocation(textureProgram, "u_directionalLight.color");
    textureUniforms.u_directionalLight.direction = glGetUniformLocation(textureProgram, "u_directionalLight.direction");

    for (int i = 0; i < 10; i++) {
        PointLightUniform& pointLight = textureUniforms.u_pointLights[i];

        char name[1024];

        std::sprintf(name, "u_pointLights[%d].color", i);
        pointLight.color = glGetUniformLocation(textureProgram, name);

        std::sprintf(name, "u_pointLights[%d].position", i);
        pointLight.position = glGetUniformLocation(textureProgram, name);

        std::sprintf(name, "u_pointLights[%d].attenuation", i);
        pointLight.attenuation = glGetUniformLocation(textureProgram, name);

        std::sprintf(name, "u_pointLights[%d].enabled", i);
        pointLight.enabled = glGetUniformLocation(textureProgram, name);
    }

    textureUniforms.u_spotLight.color = glGetUniformLocation(textureProgram, "u_spotLight.color");
    textureUniforms.u_spotLight.position = glGetUniformLocation(textureProgram, "u_spotLight.position");
    textureUniforms.u_spotLight.direction = glGetUniformLocation(textureProgram, "u_spotLight.direction");
    textureUniforms.u_spotLight.innerAngleCos = glGetUniformLocation(textureProgram, "u_spotLight.innerAngleCos");
    textureUniforms.u_spotLight.outerAngleCos = glGetUniformLocation(textureProgram, "u_spotLight.outerAngleCos");
    textureUniforms.u_spotLight.attenuation = glGetUniformLocation(textureProgram, "u_spotLight.attenuation");
    textureUniforms.u_spotLight.enabled = glGetUniformLocation(textureProgram, "u_spotLight.enabled");

    textureUniforms.u_texture = glGetUniformLocation(textureProgram, "u_texture");

    glUseProgram(textureProgram);

    glUniform1i(textureUniforms.u_texture, 0);
}

void resizeViewport() {
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);
    aspect = (float) width / (float) height;
}

void update() {
    updateControls();
    updateProjectionViewMatrix();
}

void updateControls() {
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) camera.rotateHorizontally(-glm::radians(CAMERA_ROTATION_SPEED) * dt);
    else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) camera.rotateHorizontally(glm::radians(CAMERA_ROTATION_SPEED) * dt);

    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) camera.rotateVertically(-glm::radians(CAMERA_ROTATION_SPEED) * dt);
    else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) camera.rotateVertically(glm::radians(CAMERA_ROTATION_SPEED) * dt);

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) camera.position -= camera.right * cameraMovementSpeed * dt;
    else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) camera.position += camera.right * cameraMovementSpeed * dt;

    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) camera.position -= camera.forward * cameraMovementSpeed * dt;
    else if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) camera.position += camera.forward * cameraMovementSpeed * dt;

    if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS) camera.position.y -= cameraMovementSpeed * dt;
    else if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) camera.position.y += cameraMovementSpeed * dt;

    camera.updateDirection();
}

void updateProjectionViewMatrix() {
    glm::mat4 projectionMatrix = glm::perspective(glm::radians(FOV_Y), aspect, 0.1f, 500.0f);
    glm::vec3 center = camera.position + camera.forward;
    glm::mat4 viewMatrix = glm::lookAt(camera.position, center, camera.up);
    projectionViewMatrix = projectionMatrix * viewMatrix;
}

void draw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(skyboxProgram);

    // Draw the skybox
    glUniform3fv(skyboxUniforms.u_forward, 1, (GLfloat*) &camera.forward);

    float tanY = std::tan(glm::radians(FOV_Y / 2));
    float tanX = tanY * aspect;

    glm::vec3 clipPlaneRight = tanX * camera.right;
    glm::vec3 clipPlaneUp = tanY * camera.up;

    glUniform3fv(skyboxUniforms.u_clipPlaneRight, 1, (GLfloat*) &clipPlaneRight);
    glUniform3fv(skyboxUniforms.u_clipPlaneUp, 1, (GLfloat*) &clipPlaneUp);

    skybox.draw();

    glEnable(GL_DEPTH_TEST);

    glm::mat4 modelMatrix;
    glm::mat4 normalMatrix;
    glm::mat4 matrix;

    glUseProgram(textureProgram);

    glUniform3fv(textureUniforms.u_cameraPosition, 1, (GLfloat*) &camera.position);

    glm::vec3 ambientLightColor(0.1, 0.1, 0.1);

    glUniform3fv(textureUniforms.u_ambientLight.color, 1, (GLfloat*) &ambientLightColor);

    glm::vec3 directionalLightColor(0.2, 0.1, 0.1);

    glUniform3fv(textureUniforms.u_directionalLight.color, 1, (GLfloat*) &directionalLightColor);

    // In this project the directional light direction should be normalized before setting the uniform variable
    glm::vec3 directionalLightDirection = glm::normalize(glm::vec3(0, -1, 2));

    glUniform3fv(textureUniforms.u_directionalLight.direction, 1, (GLfloat*) &directionalLightDirection);

    glm::vec3 pointLightsColor(1, 1, 0);

    for (int i = 0; i < 4; i++) {
        PointLightUniform& pointLight = textureUniforms.u_pointLights[i];

        glUniform3fv(pointLight.color, 1, (GLfloat*) &pointLightsColor);
        glUniform3fv(pointLight.position, 1, (GLfloat*) &pointLightsPositions[i]);
        glUniform1f(pointLight.attenuation, 0.5);
        glUniform1i(pointLight.enabled, true);
    }

    glUniform3f(textureUniforms.u_spotLight.color, 1, 1, 1);
    glUniform3fv(textureUniforms.u_spotLight.position, 1, (GLfloat*) &camera.position);
    glUniform3fv(textureUniforms.u_spotLight.direction, 1, (GLfloat*) &camera.forward);
    glUniform1f(textureUniforms.u_spotLight.innerAngleCos, std::cos(glm::radians(15.0f)));
    glUniform1f(textureUniforms.u_spotLight.outerAngleCos, std::cos(glm::radians(30.0f)));
    glUniform1f(textureUniforms.u_spotLight.attenuation, 0.1);
    glUniform1i(textureUniforms.u_spotLight.enabled, spotLightEnabled);

    // Draw the scene
    modelMatrix = scene.getModelMatrix();
    normalMatrix = scene.getNormalMatrix();
    matrix = projectionViewMatrix * modelMatrix;

    glUniformMatrix4fv(textureUniforms.u_modelMatrix, 1, GL_FALSE, (GLfloat*) &modelMatrix);
    glUniformMatrix4fv(textureUniforms.u_normalMatrix, 1, GL_FALSE, (GLfloat*) &normalMatrix);
    glUniformMatrix4fv(textureUniforms.u_matrix, 1, GL_FALSE, (GLfloat*) &matrix);

    scene.draw();

    // Draw the lanterns
    for (int i = 0; i < 4; i++) {
        lantern.position = lanternsPositions[i];
        lantern.rotation = lanternsRotations[i];

        modelMatrix = lantern.getModelMatrix();
        normalMatrix = lantern.getNormalMatrix();
        matrix = projectionViewMatrix * modelMatrix;

        glUniformMatrix4fv(textureUniforms.u_modelMatrix, 1, GL_FALSE, (GLfloat*) &modelMatrix);
        glUniformMatrix4fv(textureUniforms.u_normalMatrix, 1, GL_FALSE, (GLfloat*) &normalMatrix);
        glUniformMatrix4fv(textureUniforms.u_matrix, 1, GL_FALSE, (GLfloat*) &matrix);

        lantern.draw();
    }

    glDisable(GL_DEPTH_TEST);
}
